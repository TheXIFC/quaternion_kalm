/*
 * sys_main.h
 *
 *  Created on: 1 ����. 2020 �.
 *      Author: TheXIFC
 */

#ifndef INC_MAIN_SYS_MAIN_H_
#define INC_MAIN_SYS_MAIN_H_

#include "main.h"


#define RAW_MAG_16BIT_TO_mT			4912.f / 32760

#define	RAW_GY_250DPS_TO_DEG		250.f / 32768
#define	RAW_GY_500DPS_TO_DEG		500.f / 32768

#define RAW_XL_2G_TO_G				2.f / 32768

void init();
void main_loop();
void read_ASA();
void read_data();

void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim);

#endif /* INC_MAIN_SYS_MAIN_H_ */
