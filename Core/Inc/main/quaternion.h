/*
 * quaternion.h
 *
 *  Created on: 28 ���. 2019 �.
 *      Author: TheXIFC
 */

#ifndef INC_MAIN_QUATERNION_H_
#define INC_MAIN_QUATERNION_H_

#include "main.h"
#include <math.h>


typedef struct {
	double w;
	double i;
	double j;
	double k;
} Quaternion;

typedef struct
{
    double roll, pitch, yaw;
} EulerAngles;


Quaternion q_normalize(Quaternion* p);
Quaternion q_multiply(Quaternion* q, Quaternion* p);
Quaternion q_multiply_const(Quaternion* q, double c);
Quaternion q_exp(Quaternion* q);
double q_norm(Quaternion* q);
Quaternion q_add(Quaternion* q, Quaternion* p);
Quaternion q_vector_to_q(double x, double y, double z);
Quaternion q_rotate(Quaternion* v, Quaternion* q);
Quaternion q_conjugate(Quaternion* q);
EulerAngles ToEulerAngles(Quaternion q);
Quaternion ToQuaternion(double yaw, double pitch, double roll);


#endif /* INC_MAIN_QUATERNION_H_ */
