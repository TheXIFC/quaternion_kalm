/*
 * calculations.c
 *
 *  Created on: 11 ����. 2020 �.
 *      Author: TheXIFC
 */

#include <stdio.h>
#include <math.h>

#include "main.h"
#include "sys_main.h"
#include "quaternion.h"

extern double mag[3];
double mag_old[3];
double mag_sd;

extern double xl[3];
double xl_old[3];
double xl_sd;

extern double gy[3];
double gy_old[3];
double gy_sd_to_xl;
double gy_sd_to_mag;


Quaternion pos = {1, 0, 0, 0};
//Quaternion q;


void print_position() {
	EulerAngles angles = ToEulerAngles(pos);
	printf("pos x: %.3f, y: %.3f, z: %.3f\r\n", angles.roll, angles.pitch, angles.yaw);
	printf("mag x: %.3f, y: %.3f, z: %.3f\r\n", mag[0], mag[1], mag[2]);
//	printf("maq x: %.3f, y: %.3f, z: %.3f\r\n", q.i, q.j, q.k);
}


void calculate_position() {
	Quaternion g = q_vector_to_q(xl[0], xl[1], xl[2]);
	Quaternion m0 = q_vector_to_q(mag[0], mag[1], mag[2]);

	Quaternion q1;
	q1.w = sqrt((1 - g.k) / 2);
	q1.i = -g.j / sqrt(2 * (1 - g.k));
	q1.j = g.i / sqrt(2 * (1 - g.k));
	q1.k = 0;

	Quaternion m1 = q_rotate(&m0, &q1);
//	q = m1;

	Quaternion q2;
	double l = sqrt(m1.i * m1.i + m1.j * m1.j);
	double x = m1.i / l;
	q2.w = sqrt((1 + x) / 2);
	q2.i = 0;
	q2.j = 0;
//	q2.k = -sqrt((1 - x) / 2);
//	if (m1.j < 0)
//		q2.k *= -1;
	q2.k = m1.j / (2 * l * q2.w);

	pos = q_multiply(&q2, &q1);
}
