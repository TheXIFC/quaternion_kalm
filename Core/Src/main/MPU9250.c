/*
 * MPU9250.c
 *
 *  Created on: 1 ����. 2020 �.
 *      Author: TheXIFC
 */

#include "MPU9250.h"

extern I2C_HandleTypeDef hi2c1;


uint8_t read_reg(uint16_t devAddress, uint16_t memAddress) {
	uint8_t data;
	HAL_I2C_Mem_Read(&hi2c1, devAddress << 1, memAddress, I2C_MEMADD_SIZE_8BIT, &data, 1, 1-0);
	return data;
}


void read_regs(uint16_t devAddress, uint16_t memAddress, uint8_t* data, uint16_t size) {
	HAL_I2C_Mem_Read(&hi2c1, devAddress << 1, memAddress, I2C_MEMADD_SIZE_8BIT, data, size, 1-0);
}


void write_reg(uint16_t devAddress, uint16_t memAddress, uint8_t data) {
	HAL_I2C_Mem_Write(&hi2c1, devAddress << 1, memAddress, I2C_MEMADD_SIZE_8BIT, &data, 1, 100);
}


void write_regs(uint16_t devAddress, uint16_t memAddress, uint8_t* data, uint16_t size) {
	HAL_I2C_Mem_Write(&hi2c1, devAddress << 1, memAddress, I2C_MEMADD_SIZE_8BIT, data, size, 10);

}


void MPU9250_init() {
//	write_reg(MPU9250_ADDRESS, MPU9250_PWR_MGMT_1, 1 << 7);

	//enabling bypass mode
	write_reg(MPU9250_ADDRESS, MPU9250_USER_CTRL, 0);
	write_reg(MPU9250_ADDRESS, MPU9250_INT_PIN_CFG, 2);

	write_reg(MPU9250_ADDRESS, MPU9250_ACCEL_CONFIG2, 5);
	write_reg(MPU9250_ADDRESS, MPU9250_CONFIG, 2);
	return;

}


void MPU9250_get_fuse(uint8_t* fuse) {
	MPU9250_mag_set_mode(MPU9250_MAG_FUSE_ACCESS);
	read_regs(MPU9250_MAG_ADDRESS, MPU9250_MAG_ASAX, fuse, 3);
}


void MPU9250_mag_set_mode(uint8_t mode) {
	write_reg(MPU9250_MAG_ADDRESS, MPU9250_MAG_CNTL, mode);
	HAL_Delay(100);
}


void MPU9250_gy_set_scale(uint8_t scale) {
	uint8_t reg = read_reg(MPU9250_ADDRESS, MPU9250_GYRO_CONFIG);
	reg = (reg | MPU9250_GYRO_FS_SEL_MASK) & (scale << 3);
	write_reg(MPU9250_ADDRESS, MPU9250_GYRO_CONFIG, reg);
}


void MPU9250_xl_set_scale(uint8_t scale) {
	uint8_t reg = read_reg(MPU9250_ADDRESS, MPU9250_ACCEL_CONFIG);
	reg = (reg | MPU9250_ACCEL_FS_SEL_MASK) & (scale << 3);
	write_reg(MPU9250_ADDRESS, MPU9250_ACCEL_CONFIG, reg);
}


void MPU9250_read_mag_data(uint8_t* data) {
//	uint8_t st1 = read_reg(MPU9250_MAG_ADDRESS, MPU9250_MAG_ST1);
//	if (st1 == 0)
//		return;
	read_regs(MPU9250_MAG_ADDRESS, MPU9250_MAG_XOUT_L, data, 6);
	read_reg(MPU9250_MAG_ADDRESS, MPU9250_MAG_ST2);
}


void MPU9250_read_gy_data(uint8_t* data) {
	uint8_t buffer[6];
	read_regs(MPU9250_ADDRESS, MPU9250_GYRO_XOUT_H, buffer, 6);
	for (int i = 0; i < 3; ++i) {
		data[2*i] = buffer[2*i + 1];
		data[2*i + 1] = buffer[2*i];
	}
}


void MPU9250_read_xl_data(uint8_t* data) {
	uint8_t buffer[6];
	read_regs(MPU9250_ADDRESS, MPU9250_ACCEL_XOUT_H, buffer, 6);
	for (int i = 0; i < 3; ++i) {
		data[2*i] = buffer[2*i + 1];
		data[2*i + 1] = -buffer[2*i];
	}
}
