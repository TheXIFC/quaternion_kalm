/*
 * sys_main.c
 *
 *  Created on: 1 ����. 2020 �.
 *      Author: TheXIFC
 */

#include <stdio.h>
#include <math.h>

#include "sys_main.h"
#include "MPU9250.h"
#include "main.h"
#include "calculations.h"


extern I2C_HandleTypeDef hi2c1;
extern TIM_HandleTypeDef htim1;

uint32_t system_tick;


double ASA[3];
double mag[3];

int16_t mag_max[3] = {-32000, -32000, -32000};
int16_t mag_min[3] = {32000, 32000, 32000};
//-80 380 90
int16_t mag_shift[3];
int16_t raw_magb[3];


double xl[3];
int16_t xl_raw[3];
int16_t xl_shift[3];

int16_t xl_array[3][100];
int16_t xl_av[3];
int16_t xl_sd_raw[3];


double gy[3];
int16_t gy_raw[3];
int16_t gy_shift[3];


int16_t gy_array[3][100];
int16_t gy_av[3];
int16_t gy_sd_raw[3];

uint16_t counter = 0;

void init() {
	HAL_Delay(100);
	MPU9250_init();
	read_ASA();
	MPU9250_mag_set_mode(MPU9250_MAG_SCALE_16BIT | MPU9250_MAG_CONT_100HZ);
	MPU9250_xl_set_scale(MPU9250_FULL_SCALE_2G);
	MPU9250_gy_set_scale(MPU9250_GYRO_FULL_SCALE_250DPS);


//	for (uint8_t i = 0; i < 128; i++) {
//		uint8_t data;
//		HAL_StatusTypeDef st = HAL_I2C_Mem_Read(&hi2c1, i << 1, 0x00, I2C_MEMADD_SIZE_8BIT, &data, 1, 10);
//		if (st == HAL_OK)
//			printf("%d\r\n", i);
//	}


	HAL_TIM_Base_Start_IT(&htim1);
}



void main_loop() {

	while (1) {
		HAL_Delay(1000);
		print_position();

//		printf("ASA x: %.3f, y: %.3f, z: %.3f\r\n", ASA[0], ASA[1], ASA[2]);

//		printf("mag x: %.3f, y: %.3f, z: %.3f\r\n", mag[0], mag[1], mag[2]);
//		printf("mag x: %5d, y: %5d, z: %5d\r\n", raw_magb[0], raw_magb[1], raw_magb[2]);
//		printf("shift x: %5d, y: %5d, z: %5d\r\n", mag_shift[0], mag_shift[1], mag_shift[2]);

//		printf("xl raw x: %5d, y: %5d, z: %5d\r\n", xl_raw[0], xl_raw[1], xl_raw[2]);
//		printf("xl x: %.3f, y: %.3f, z: %.3f\r\n", xl[0], xl[1], xl[2]);
//		printf("xl av x: %5d, y: %5d, z: %5d\r\n", xl_av[0], xl_av[1], xl_av[2]);
//		printf("xl sd x: %5d, y: %5d, z: %5d\r\n", xl_sd_raw[0], xl_sd_raw[1], xl_sd_raw[2]);

//		printf("gy raw x: %5d, y: %5d, z: %5d\r\n", gy_raw[0], gy_raw[1], gy_raw[2]);
//		printf("gy x: %.3f, y: %.3f, z: %.3f\r\n", gy[0], gy[1], gy[2]);
//		printf("gy av x: %5d, y: %5d, z: %5d\r\n", gy_av[0], gy_av[1], gy_av[2]);
//		printf("gy sd x: %5d, y: %5d, z: %5d\r\n", gy_sd_raw[0], gy_sd_raw[1], gy_sd_raw[2]);

//		printf("%d\r\n", system_tick);
	}
}


void read_ASA() {
	uint8_t fuse_raw[3];
	MPU9250_get_fuse(fuse_raw);

	for (int i = 0; i < 3; ++i) {
		ASA[i] = fuse_raw[i] / 256.f + 0.5;
	}
}



void read_data() {
	int16_t raw_mag[3];
	MPU9250_read_mag_data((uint8_t*) raw_mag);

	//changing mag axis
	int16_t tmp = raw_mag[0];
	raw_mag[0] = raw_mag[1];
	raw_mag[1] = tmp;
	raw_mag[2] = -raw_mag[2];

	for (int i = 0; i < 3; ++i) {
		mag[i] = (raw_mag[i] - mag_shift[i]) * RAW_MAG_16BIT_TO_mT * ASA[i];
		raw_magb[i] = raw_mag[i] - mag_shift[i];

		if (raw_mag[i] > mag_max[i])
			mag_max[i] = raw_mag[i];
		else if (raw_mag[i] < mag_min[i])
			mag_min[i] = raw_mag[i];

		mag_shift[i] = (mag_max[i] + mag_min[i]) / 2;
	}


//	int16_t raw_xl[3];
	MPU9250_read_xl_data((uint8_t*) xl_raw);
	for (int i = 0; i < 3; ++i) {
		xl[i] = xl_raw[i] * RAW_XL_2G_TO_G;
		xl_array[i][counter] = xl_raw[i];
	}


	MPU9250_read_gy_data((uint8_t*) gy_raw);
	for (int i = 0; i < 3; ++i) {
		gy[i] = gy_raw[i] * RAW_GY_250DPS_TO_DEG;
		gy_array[i][counter] = gy_raw[i];
	}


	counter += 1;
	if (counter >= 100) {
		counter = 0;
		for (int i = 0; i < 3; ++i) {
			int32_t gy_sum = 0;
			int32_t xl_sum = 0;
			for (int j = 0; j < 100; ++j) {
				gy_sum += gy_array[i][j];
				xl_sum += xl_array[i][j];
			}

			gy_av[i] = gy_sum / 100;
			xl_av[i] = xl_sum / 100;

			gy_sum = xl_sum = 0;
			for (int j = 0; j < 100; ++j) {
				gy_sum += (gy_array[i][j] - gy_av[i]) * (gy_array[i][j] - gy_av[i]);
				xl_sum += (xl_array[i][j] - xl_av[i]) * (xl_array[i][j] - xl_av[i]);
			}

			gy_sd_raw[i] = sqrt(gy_sum / 100);
			xl_sd_raw[i] = sqrt(xl_sum / 100);
		}
	}
}



void HAL_TIM_PeriodElapsedCallback (TIM_HandleTypeDef *htim) {
	++system_tick;
	if ((system_tick % 10) == 0) {
		read_data();
		calculate_position();
	}
}
