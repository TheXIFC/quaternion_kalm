/*
* * quaternion.c
 *
 *  Created on: 28 ���. 2019 �.
 *      Author: TheXIFC
 */

#include "quaternion.h"

Quaternion q_normalize(Quaternion* q) {
	Quaternion r = *q;
	double n = 0.f;
	double *rd = (double*) &r;
	for (uint8_t i = 0; i < 4; i++) {
		n += rd[i] * rd[i];
	}
	n = sqrt(n);

	for (uint8_t i = 0; i < 4; i++) {
		rd[i] /= n;
	}

	return r;
}


Quaternion q_multiply(Quaternion* q, Quaternion* p) {
	Quaternion r;

	r.w = q->w * p->w - q->i * p->i - q->j * p->j - q->k * p->k;
	r.i = q->w * p->i + q->i * p->w + q->j * p->k - q->k * p->j;
	r.j = q->w * p->j + q->j * p->w + q->k * p->i - q->i * p->k;
	r.k = q->w * p->k + q->k * p->w + q->i * p->j - q->j * p->i;

	return r;
}


Quaternion q_multiply_const(Quaternion* q, double c) {
	Quaternion r = *q;
	double *rd = (double*) &r;
	for (uint8_t i = 0; i < 4; i++) {
		rd[i] *= c;
	}
	return r;
}


Quaternion q_exp(Quaternion* q) {
	Quaternion r;
	double norm = q_norm(q);
	r.w = exp(q->w) * cos(norm);
	double k;
	if (norm == 0)
		k = exp(q->w);
	else
		k = exp(q->w) * sin(norm) / norm;
	r.i = q->i * k;
	r.j = q->j * k;
	r.k = q->k * k;
	return r;
}


double q_norm(Quaternion* q) {
	double norm = 0;
	double *qd = (double*) q;
	for (uint8_t i = 0; i < 4; i++) {
		norm += qd[i] * qd[i];
	}
	return sqrt(norm);
}


Quaternion q_add(Quaternion* q, Quaternion* p) {
	Quaternion r;
	r.w = q->w + p->w;
	r.i = q->i + p->i;
	r.j = q->j + p->j;
	r.k = q->k + p->k;
	return r;
}


Quaternion q_vector_to_q(double x, double y, double z) {
	Quaternion r;
	double *rd = (double*) &r;
	rd[0] = 0;
	rd[1] = x;
	rd[2] = y;
	rd[3] = z;
	return q_normalize(&r);
}


Quaternion q_rotate(Quaternion* v, Quaternion* q) {
	Quaternion q1 = q_multiply(q, v);
	Quaternion q2 = q_conjugate(q);
	return q_multiply(&q1, &q2);
}


Quaternion q_conjugate(Quaternion* q) {
	Quaternion r;
	double *rd = (double*) &r;
	double *qd = (double*) q;
	rd[0] = qd[0];
	for (int i = 1; i < 4; i++)
		rd[i] = -qd[i];
	return r;
}

EulerAngles ToEulerAngles(Quaternion q)
{
    EulerAngles angles;

    // roll (x-axis rotation)
    double sinr_cosp = +2.0 * (q.w * q.i + q.j * q.k);
    double cosr_cosp = +1.0 - 2.0 * (q.i * q.i + q.j * q.j);
    angles.roll = atan2(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
    double sinp = +2.0 * (q.w * q.j - q.k * q.i);
    if (fabs(sinp) >= 1)
        angles.pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        angles.pitch = asin(sinp);

    // yaw (z-axis rotation)
    double siny_cosp = +2.0 * (q.w * q.k + q.i * q.j);
    double cosy_cosp = +1.0 - 2.0 * (q.j * q.j + q.k * q.k);
    angles.yaw = atan2(siny_cosp, cosy_cosp);

    return angles;
}


Quaternion ToQuaternion(double yaw, double pitch, double roll) // yaw (Z), pitch (Y), roll (X)
{
    // Abbreviations for the various angular functions
    double cy = cos(yaw * 0.5);
    double sy = sin(yaw * 0.5);
    double cp = cos(pitch * 0.5);
    double sp = sin(pitch * 0.5);
    double cr = cos(roll * 0.5);
    double sr = sin(roll * 0.5);

    Quaternion q;
    q.w = cy * cp * cr + sy * sp * sr;
    q.i = cy * cp * sr - sy * sp * cr;
    q.j = sy * cp * sr + cy * sp * cr;
    q.k = sy * cp * cr - cy * sp * sr;

    return q;
}
